from rest_framework import status
from rest_framework.response import Response
from django.contrib.auth import login
from .serializers import UserSerializer, LoginSerializer
from django.contrib.auth import authenticate, login
from rest_framework.views import APIView
from .crypto import add_claim_to_JWT_token
from rest_framework_simplejwt.tokens import RefreshToken



class LoginAPI(APIView):
    """
    Login endpoint, returns user and tokens
    Input: email, password
    1. check user credentials
    2. check if user is activated 
    4. login user => generate and return tokens
    """
    serializer_class = LoginSerializer

    def get_serializer_context(self):
        return {"request": self.request}
    
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data.get("email")
        password = serializer.validated_data.get("password")
      
        # 1 & 2. check user credentials (returns none for incorrect credentials or inactive user)
        user = authenticate(request, email=email, password=password)
 
        if user is not None:
            login(request, user)
            refresh = RefreshToken.for_user(user)
         
            response_data = {
                "user": UserSerializer(user).data,
                "access_token": str(refresh.access_token),  
                "refresh_token": str(refresh)  
            }
            return Response(response_data)
        else:
            # Handle authentication failure
            return Response({"error": "Invalid credentials"}, status=status.HTTP_401_UNAUTHORIZED)

