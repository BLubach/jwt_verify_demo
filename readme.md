# Simple JWT auth system
This is a simple project/system form myself as a template. It's not complete nor production ready by any means. 

1) auth server that generates tokens
2) Django data app that uses custom middleware classes for global authorization
3) Django data app use of custom permission classes for more flexible use of authorization
4) spring boot app using spring security to check jwt similar to custom middleware (app 2)


The system is a very basic system where an auth server stores user data and signs token. The data servers are split based on an examplary field "Company", but could be any type of field. It showcases a hard data split where company data is stored separately (for security reasons or other). 


## Token validation
Checking tokens can be done in multiple ways, for this app we have multiple methods. All are based on django, but with a little work can be used by other frameworks. Common method can be applied, but most check for the existance of a corresponding user(which we dont have in the company databases)


Here 4 apps are inside the docker. 
1. Django authServer (app)
-  generates JWT tokens
-  adds a "company" field to said token 

2. Django data app for "Company A" under the name "middlewareJWTverify". As the name suggests, uses middleware to verify token validity and matches company 
- Custom middleware to check token and company

3. Django data app for "Company B" 
- Custom middleware to check token 
- Custom authentication class to check company

4. Spring boot app for "Company C" 
- Spring security filters that check the jwt tokens 


More technical details can be found inside the Readme.md file of each project. 






# Docker setup 
1) Auth server
- sqlite3.db database (stored inside github for convenience)

2) CompanyA server 
- sqlite3.db database (stored inside github for convenience)

3) CompanyB server
- sqlite3.db database (stored inside github for convenience)

4) CompanyC server
- postgressdb inside docker
- commandlinerunner to create test entries 



## networks 
All apps run on the same bridge network: microserv-jwttest-network
Spring-boot app + postgress database run on a sidenetwork: springboot-microservice-network

This ensures that all apps are accessible for nginx with container name instead of porting to localhost AND the postgress database is only visible for the spring-boot api. 


## Running the app 

``` 
docker-compose up -d --build
```  

Databases (mostly sqlite) are in the source code for testing, in production this needs to be moved to a separate database. 


### Accounts 
``` 
# Auth admin account 
email: admin@gmail.com
password: admin

# Company A account
companya@user.com
companya

# Company B account
companyb@user.com
companyb

# Company C account
companyc@user.com 
companyc
``` 








