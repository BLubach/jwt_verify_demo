package com.bouke.security.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.nio.file.Files;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.function.Function;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import java.nio.file.Paths;
import java.security.KeyFactory;
import java.util.Base64;

@Service
public class JwtService {

  // @Value("${application.security.jwt.secret-key}")
  // private String secretKey;
  // @Value("${application.security.jwt.expiration}")
  // private long jwtExpiration;
  // @Value("${application.security.jwt.refresh-token.expiration}")
  // private long refreshExpiration;
  
  @Value("${application.security.allowed-company}")
  private String allowedCompany; 

  @Value("${application.resource-dir}")
  private String resourceFolder;


  private PublicKey publicKey;

  // Extract email from the token
  public String extractEmail(String token) {
    return extractClaim(token, claims -> claims.get("email", String.class));
  }

  // Extract company from the token
  public String extractCompany(String token) {
    return extractClaim(token, claims -> claims.get("company", String.class));
  }

  // Extract specific claims from the token
  public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = extractAllClaims(token);
    return claimsResolver.apply(claims);
  }

  // Check if token is valid
  public boolean isTokenValid(String token) {
    return !isTokenExpired(token);
  }

  // Check if token is expired
  private boolean isTokenExpired(String token) {
    return extractExpiration(token).before(new Date());
  }

  // Extract expiration date from token
  private Date extractExpiration(String token) {
    return extractClaim(token, Claims::getExpiration);
  }

  // Extract all claims from the token
  private Claims extractAllClaims(String token) {
    return Jwts
        .parserBuilder()
        .setSigningKey(getPublicKey())
        .build()
        .parseClaimsJws(token)
        .getBody();
  }

  public boolean iSCorrectCompany(String token){ 
    if (allowedCompany.equals(extractCompany(token))) {
      return true;
    }
    return false;
  }


  // Lazy initialization of the public key
  private PublicKey getPublicKey()  {
    if (publicKey == null) {
      try {
        // Read the PEM file into a string
        // String key = new String(Files.readAllBytes(Paths.get("src/main/resources/keys/public_key.pem")));
        String key = new String(Files.readAllBytes(Paths.get(resourceFolder + "/keys/public_key.pem")));

        // Remove the header, footer, and line breaks
        key = key.replace("-----BEGIN PUBLIC KEY-----", "")
            .replace("-----END PUBLIC KEY-----", "")
            .replaceAll("\\s", "");

        // Decode Base64 content
        byte[] keyBytes = Base64.getDecoder().decode(key);

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        publicKey = keyFactory.generatePublic(spec);
      } catch (Exception e) {
        throw new RuntimeException("Could not read public key", e);
      }
    }
    return publicKey;
  }
}
