

## Django app: Custom middleware
Middleware is used as an intermediate between the incoming request and the url.py file. This means that all requests will be blocked that do not have a token in the header. This is only possible when there is a separate 'auth service' that generates tokens as a login request does not have a token. There are ways to exclude certain endpoints from a middleware call, but I won't go into further detail on how to do this as this is not necessary for the current app. 


Folder stucture
mainapp
- core
   #... other files
   - settings.py                 <-- update 
- middleware
   - JWTauth.py                 <-- new middleware class
- cars
  #... other files
  - permissions.py              <-- new file with permissions


```
# settings.py


#... other settings  
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'middleware.JWTauth.JWTAuthentication',                      <--- insert custom middleware file 
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
]
#... other settings  

```

The custom middleware is set in JWTAuth.py file, this checks: 
- Token verification, if the correct (private) signing key is used
- Token expiration
- If company in the token matches this apps company. Because we verified the token signature we know the claims in the tokens aren't tampered with. 

```
# in JWTAuth.py

import jwt
from django.http import JsonResponse
import os


class JWTAuthentication:
    def __init__(self, get_response):
        self.get_response = get_response
        self.key = os.environ.get('JWT_KEY')
        self.company = os.environ.get('COMPANY')

    def __call__(self, request):
        token = request.headers.get('Authorization')

        if not token or not token.startswith('Bearer '):
            return JsonResponse({'error': 'Invalid token'}, status=401)

        token = token.split('Bearer ')[1]

        try:
            decoded = jwt.decode(token,self.key, algorithms=["HS256"])
            request.payload = decoded
            
            # check if company in token payload is the same as the one in the server
            if request.payload.get("company") != self.company:
                return JsonResponse({'error': 'Not authorized'}, status=403)

        except (jwt.ExpiredSignatureError, jwt.InvalidTokenError):
            return JsonResponse({'error': 'Token expired'}, status=401)

        response = self.get_response(request)
        return response

```


Because the middleware checks if the authorization jwt token is valid and matches this apps company we don't need to do any auth in the view. 

```
# car/views.py file

from rest_framework import generics
from .models import Car
from .serializers import CarSerializer
from .permissions import CheckCompanyPermission

class CarListView(generics.ListAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer


```
