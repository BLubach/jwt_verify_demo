package com.bouke.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


import com.bouke.security.car.CarRequest;
import com.bouke.security.car.CarService;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class SecurityApplication {



	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(CarService carService) {
		return args -> {
			CarRequest carRequest = CarRequest.builder()
					.name("BMW")
					.color("Black")
					.price(100000)
					.build();
			carService.save(carRequest);

			CarRequest carRequest2 = CarRequest.builder()
					.name("Audi")
					.color("White")
					.price(120000)
					.build();

			carService.save(carRequest2);

		};
	}

}
