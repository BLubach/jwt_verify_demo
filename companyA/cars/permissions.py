from rest_framework.permissions import BasePermission
import os


class CheckCompanyPermission(BasePermission):
    """
    Permission check for company in token payload
    """

    # add message
    message = "You are not allowed to access data from this company"

    def has_permission(self, request, view):

        # get company from server environment variables
        server_company = os.environ.get('COMPANY')
       
        # check if payload exists
        try:
            request.payload.get("company")
          
            if request.payload.get("company") != server_company:
                # if company in payload does not match server company, allow access
                return False
            else:
                # if company in payload matches server company, allow access
                return True
        except AttributeError:
            # if payload does not exist, allow access
            return False
