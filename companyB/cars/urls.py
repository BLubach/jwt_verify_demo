from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from .views import CarListCreateView

urlpatterns = [
    path('cars/', CarListCreateView.as_view(), name='car_list'),
]
