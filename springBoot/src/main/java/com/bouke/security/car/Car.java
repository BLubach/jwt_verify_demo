package com.bouke.security.car;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Car {

        @Id
        @GeneratedValue
        private Integer id;

        @Column(nullable = false)
        private String name;

        @Column(nullable = false)
        private String color;

        @Column(nullable = false)
        private Integer price;


        
        @CreatedDate
        @Column(nullable = false, updatable = false)
        private LocalDateTime created_at;

        @LastModifiedDate
        @Column(insertable = false)
        private LocalDateTime updated_at;

  
}
