// package com.bouke.security.unit;

// import org.mockito.InjectMocks;
// import org.mockito.Mock;

// import com.bouke.security.user.UserRepository;
// import com.bouke.security.exception.DuplicateFieldException;
// import com.bouke.security.exception.ResourceNotFoundException;

// import org.junit.jupiter.api.Test;
// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.extension.ExtendWith;
// import org.springframework.test.context.junit.jupiter.SpringExtension;

// import com.bouke.security.user.UserServiceImpl;
// import com.bouke.security.user.domain.User;

// import java.util.Arrays;
// import java.util.List;
// import java.util.Optional;

// import static org.mockito.Mockito.verify;
// import static org.mockito.Mockito.when;
// import static org.junit.jupiter.api.Assertions.assertEquals;
// import static org.junit.jupiter.api.Assertions.assertThrows;
// import static org.junit.jupiter.api.Assertions.assertTrue;
// import static org.mockito.Mockito.never;
// import static org.mockito.Mockito.times;

// @ExtendWith(SpringExtension.class)
// public class UserServiceTests {

//     @Mock
//     private UserRepository userRepository;

//     @InjectMocks
//     private UserServiceImpl userService;

//     private User user;
//     private User existingUser;

//     @BeforeEach
//     public void setUp() {
//         user = new User();
//         user.setEmail("user1@email.com");
//         user.setPassword("password1");

//         existingUser = new User();
//         existingUser.setEmail("existing@user.com");
//         existingUser.setPassword("password");

//         // mock userRepository
//         when(userRepository.existsByEmail(user.getEmail())).thenReturn(false);
//         when(userRepository.save(user)).thenReturn(user);
//         when(userRepository.existsByEmail(existingUser.getEmail())).thenReturn(true);

//         when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
//         when(userRepository.findByEmail("nonexistinguser@gmail.com")).thenReturn(Optional.empty());

//     }

//     @Test
//     public void testSaveUser() {

//         User savedUser = userService.save(user);

//         verify(userRepository, times(1)).save(user);
//         assertEquals(user, savedUser);
//     }

//     @Test
//     public void testSaveUser_DuplicateEmail() {
//         // Attempt to save the existing user
//         DuplicateFieldException exception = assertThrows(DuplicateFieldException.class, () -> {
//             userService.save(existingUser);
//         });

//         // Verify that userRepository.save was not called
//         verify(userRepository, never()).save(existingUser);

//         // Assert the exception message
//         String expectedMessage = "Email already exists";
//         String actualMessage = exception.getMessage();
//         assertTrue(actualMessage.contains(expectedMessage));
//     }

//     @Test
//     public void testFindByEmail_success() {
//         // Call the service method
//         User foundUser = userService.findByEmail(user.getEmail());

//         verify(userRepository, times(1)).findByEmail(user.getEmail());

//         // Assert that the user was found
//         assertEquals(user, foundUser);

//     }

//     @Test
//     public void testFindByEmail_noUserExists() {
//         // Call the service method and expect a ResourceNotFoundException
//         assertThrows(ResourceNotFoundException.class, () -> {
//             userService.findByEmail("nonexistinguser@gmail.com");
//         });

//         // Verify repository method was called once with the correct email
//         verify(userRepository, times(1)).findByEmail("nonexistinguser@gmail.com");
//     }
// }
