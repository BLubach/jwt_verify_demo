package com.bouke.security.car;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class ColorOptionsConverter implements AttributeConverter<ColorOptions, String> {

    @Override
    public String convertToDatabaseColumn(ColorOptions attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.getDisplayName();
    }

    @Override
    public ColorOptions convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.isEmpty()) {
            return null;
        }
        return ColorOptions.fromDisplayName(dbData);
    }
}
