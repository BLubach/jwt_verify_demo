package com.bouke.security.config;

import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.Arrays;

// added MvcRequestMatcher.Builder bean to use in securityFilterChain
// ensures we can specify both antMatchers and mvcMatchers in the same security configuration
// antmatchers used for h2 console
// differentiates between antMatchers and mvcMatchers required by update https://spring.io/security/cve-2023-34035 
// this class is used as a utility class to create mvcMatchers for both single strings and arrays of strings

public class MvcRequestMatcherUtil {

    private final HandlerMappingIntrospector introspector;

    public MvcRequestMatcherUtil(HandlerMappingIntrospector introspector) {
        this.introspector = introspector;
    }

    public RequestMatcher pattern(String url) {
        return new MvcRequestMatcher(introspector, url);
    }

    public RequestMatcher[] patterns(String[] urls) {
        return Arrays.stream(urls)
                .map(url -> new MvcRequestMatcher(introspector, url))
                .toArray(RequestMatcher[]::new);
    }
}