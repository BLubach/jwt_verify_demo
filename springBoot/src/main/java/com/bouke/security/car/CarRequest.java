package com.bouke.security.car;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CarRequest {

    private Integer id;
    private String name;
    private String color;
    private Integer price;
}
