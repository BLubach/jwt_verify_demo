import jwt
from django.conf import settings
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.authentication import BaseAuthentication


class JWTAuthentication(BaseAuthentication):
    def authenticate(self, request):
        auth_header = request.headers.get('Authorization')

        if not auth_header or not auth_header.startswith('Bearer '):
            return None

        token = auth_header.split(' ')[1]
        path_to_public_key = settings.PUBLIC_KEY_PATH

        if not path_to_public_key:
            raise AuthenticationFailed('Path to public key file not configured.')

        try:
            with open(path_to_public_key, 'r') as f:
                public_key = f.read()

            payload = jwt.decode(token, public_key, algorithms=['RS256'])
            request.payload = payload

            # Extract user information from the payload
            user_info = {
                'email': payload.get('email'),
                'company': payload.get('company') 
            }


            return (user_info, token)

        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Token expired')
        except jwt.InvalidTokenError:
            raise AuthenticationFailed('Invalid token')