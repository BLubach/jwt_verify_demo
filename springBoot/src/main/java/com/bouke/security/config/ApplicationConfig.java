package com.bouke.security.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import com.bouke.security.auditing.ApplicationAuditAware;

@Configuration
@RequiredArgsConstructor
public class ApplicationConfig {



  @Bean
  public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
    return config.getAuthenticationManager();
  }



  @Bean
  public AuditorAware<String> auditorAware() {
    return new ApplicationAuditAware();
  }
}
