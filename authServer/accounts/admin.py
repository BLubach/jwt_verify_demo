from django.contrib import admin
from .models import User

admin.site.site_header = "authServer Admin"
admin.site.site_title = "authServer Admin Portal"

admin.site.register(User)