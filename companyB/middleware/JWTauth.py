import jwt
from django.conf import settings
from django.http import JsonResponse
from django.utils.deprecation import MiddlewareMixin
from rest_framework.exceptions import AuthenticationFailed
import os 


class JWTAuthenticationMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        if request.path.startswith('/admin'):
            return self.get_response(request)
        token = request.headers.get('Authorization')

        if not token or not token.startswith('Bearer '):
            return JsonResponse({'error': 'Invalid token'}, status=401)

        token = token.split('Bearer ')[1]
        path_to_public_key = settings.PUBLIC_KEY_PATH

        if not path_to_public_key:
                raise AuthenticationFailed('Path to public key file not configured.')

        with open(path_to_public_key, 'r') as file:
            public_key = file.read()

        try:
            payload = jwt.decode(token, public_key, algorithms=['RS256'])
            
            server_company = os.environ.get('COMPANY')

            # check if the token payload contains companyB access
            if payload.get("company") != server_company:
                return JsonResponse({'error': 'You are not allowed to access data from this company'}, status=403)

            request.payload = payload


        except (jwt.ExpiredSignatureError, jwt.InvalidTokenError, FileNotFoundError) as e:
            return JsonResponse({'error': str(e)}, status=401)

        response = self.get_response(request)
        return response