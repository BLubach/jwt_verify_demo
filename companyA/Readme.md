

## Django app: Create custom permission class
In this app a custom JWT auth class is created which can be called inside a view. This is split into 2 parts, first we will check validity of the token in middleware and set the payload data into the context. Next we use a custom permission class to check whether the company is related to a token. 

Folder stucture
mainapp
- core
  - settings.py                 <-- update 
- middleware
   - JWTauth.py                 <-- updated middleware class
- cars
  - permissions.py              <-- new file with permissions


Update the settings.py file, first we set the default auth class. Then we add SIMPLE_JWT settings, specifically the algorithm and verifying key. The signing key will not be use by the Main app, only by the auth app. There are many more settings used by the simplejwt package, more information can be found in the package documentation. 

```
# settings.py

#... other settings  
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'middleware.JWTauth.JWTAuthentication',                     <--- insert custom middleware file 
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
]
#... other settings  

```


#in customJWTauth/jwtauthfromfile.py


```
class JWTAuthentication(BaseAuthentication):
    def authenticate(self, request):
        auth_header = request.headers.get('Authorization')

        if not auth_header or not auth_header.startswith('Bearer '):
            return None

        token = auth_header.split(' ')[1]
        path_to_public_key = settings.PUBLIC_KEY_PATH

        if not path_to_public_key:
            raise AuthenticationFailed('Path to public key file not configured.')

        try:
            with open(path_to_public_key, 'r') as f:
                public_key = f.read()

            payload = jwt.decode(token, public_key, algorithms=['RS256'])
            request.payload = payload

            # Extract user information from the payload
            user_info = {
                'email': payload.get('email'),
                'company': payload.get('company') 
            }


            return (user_info, token)

        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Token expired')
        except jwt.InvalidTokenError:
            raise AuthenticationFailed('Invalid token')

```


Next a custom permission class will decode the oken and check the token payload for the correct company. This could have also been done inside the custom middleware, however, because we only check this inside the custom permissions class we can also add endpoints that might be accessible for other company users.  


```
# in permissions.py
from rest_framework.permissions import BasePermission
import os 

class CheckCompanyPermission(BasePermission):
    """
    Permission check for company in token payload
    """

    # add message
    message = "You are not allowed to access data from this company"
    def has_permission(self, request, view):

        # get company from server environment variables
        server_company = os.environ.get('COMPANY')

        if request.payload.get("company") != server_company:

            return False
        
        
        return True

```



Inside the view, the CheckCompanyPermission class is called to check for the company. 
```
# car/views.py file

from rest_framework import generics
from .models import Car
from .serializers import CarSerializer
from .permissions import CheckCompanyPermission

class CarListView(generics.ListAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer
    permission_classes = [CheckCompanyPermission]

```