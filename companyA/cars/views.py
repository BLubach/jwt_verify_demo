from rest_framework import generics
from .models import Car
from .serializers import CarSerializer
from .permissions import CheckCompanyPermission

class CarListCreateView(generics.ListCreateAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer
    permission_classes = [
        CheckCompanyPermission
    ]
