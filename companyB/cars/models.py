from django.db import models


class Car(models.Model):

    name = models.CharField(max_length=100)
    licence_plate = models.CharField(max_length=100)

    class ColorOptions(models.TextChoices):
        RED = 'Red'
        BLUE = 'Blue'
        GREEN = 'Green'
        YELLOW = 'Yellow'
        BLACK = 'Black'
        WHITE = 'White'

    color = models.CharField(max_length=32, choices=ColorOptions.choices, default=ColorOptions.RED)
    price = models.IntegerField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.name