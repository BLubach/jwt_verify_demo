package com.bouke.security.car;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarService {

    private final CarRepository repository;

    public void save(CarRequest request) {
        var car = Car.builder()
                .id(request.getId())
                .name(request.getName())
                .color(request.getColor())
                .price(request.getPrice())
                .build();
        repository.save(car);
    }

    public List<Car> findAll() {
        return repository.findAll();
    }
}
