
import jwt
from django.conf import settings
from rest_framework.exceptions import AuthenticationFailed


def add_claim_to_JWT_token(token, claim, value) -> str:
    """
    Add a claim to a JWT token
    """
    path_to_private_key = settings.PRIVATE_KEY_PATH

    # check if path to private key is configured
    if not path_to_private_key:
        raise AuthenticationFailed('Path to private key file not configured.')

    try:
        with open(path_to_private_key, 'r') as f:
            private_key = f.read()

        
        # decode the token
        decodeJWT = jwt.decode(token, private_key, algorithms=['RS256'])

        # add the claim to the token
        decodeJWT[claim] = value

        # encode the token
        encodedJWT = jwt.encode(decodeJWT, settings.private_key, algorithm="RS256")

        # return the encoded token
        return encodedJWT
    

    # handle exceptions
    except jwt.ExpiredSignatureError:
        raise AuthenticationFailed('Token has expired.')
    except jwt.InvalidTokenError:
        raise AuthenticationFailed('Invalid token.')
    except Exception as e:
        print(e)
        raise AuthenticationFailed('Failed to authenticate.')
    
