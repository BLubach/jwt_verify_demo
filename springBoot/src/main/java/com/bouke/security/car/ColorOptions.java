package com.bouke.security.car;


public enum ColorOptions {
    RED("Red"),
    BLUE("Blue"),
    GREEN("Green"),
    YELLOW("Yellow"),
    BLACK("Black"),
    WHITE("White");

    private final String displayName;

    ColorOptions(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static ColorOptions fromDisplayName(String displayName) {
        for (ColorOptions color : ColorOptions.values()) {
            if (color.displayName.equalsIgnoreCase(displayName)) {
                return color;
            }
        }
        throw new IllegalArgumentException("Unknown color: " + displayName);
    }
}